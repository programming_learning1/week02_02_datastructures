﻿using System.ComponentModel.Design;

Console.WriteLine();


Console.WriteLine("================================ CLASSROOM_DATA_STRUCTURES =====================================");
Console.WriteLine();
Console.WriteLine("----------------------------------------- arrays ------------------------------------");

/*int[] numbersArray;
int[] numbersArray = new int[6];
int[] numbersArray = { 3, 5, 2, 13, 7, 10 };*/
int[] numbersArray = new int[5] { 5, 2, 6, 8, 1 };

Console.WriteLine(numbersArray[3]);Console.WriteLine();
numbersArray[3] = 42;
Console.WriteLine(numbersArray[3]);

int x = numbersArray[2];
int y = numbersArray.ElementAt(2);
Console.WriteLine($"x={x}");
Console.WriteLine($"y={y}");

// bi-dimensional array
// 1 2 3 => linia 0 + coloanele 0, 1, 2
// 4 5 6 => linia 1 + coloanele 0, 1, 2
int[,] matrix = new int[2, 3];//bidimensional array [,]
matrix[0, 0] = 1;
matrix[0, 1] = 2;
matrix[0, 2] = 3;
matrix[1, 0] = 4;
matrix[1, 1] = 5;
matrix[1, 2] = 6;
Console.WriteLine(matrix[1, 1]);
matrix[1, 1] = 12344567;
Console.WriteLine(matrix[1, 1]);

// tri-dimensional array
int[,,] tridimensionalArray = new int[2, 3, 1];//tridimensional array [,,]
tridimensionalArray[0, 0, 0] = 1;

string[] words = new string[5] { "5", "2, 6, 8, 1, !!!, ??, ana", "ana", "pisica", "condition" };
Console.WriteLine(words[2]);
Console.WriteLine(words[1]);

// class assignement
//1
//Declare an array that will have 5 integer elements (12, 14, 34, 567, 34).
//Add those 5 elements into the array one by one. Print the value of the second element in the array.
Console.WriteLine();
Console.WriteLine("Ex1");
int[] n1 = new int[5];
n1[0] = 12;
n1[1] = 14;
n1[2] = 34;
n1[3] = 567;
n1[4] = 34;
Console.WriteLine(n1[1]);

//2
//Store and print the sum of the first three elements of the array and the product the last two elements in the array.
//Print the sum of the two results above (sum + product).
// sum = 60, prod = 19278
// sum2 = 19338
Console.WriteLine();
Console.WriteLine("Ex2");
int suma = n1[0] + n1[1] + n1[2];
int prod = n1[3] * n1[4];
Console.WriteLine("sum=" + suma + " prod=" + prod);
int suma2 = suma + prod;
Console.WriteLine("sum=" + suma2);

Console.WriteLine();
Console.WriteLine("-------------------------------------- lists --------------------------------------------");

//List<int> numbersList;
//List<int> numbersList = new List<int>();
List<int> numbersList = new List<int> { 3, 5, 2, 13, 7, 10 };
numbersList[3] = 42;

x = numbersList[2];
y = numbersList.ElementAt(2);

numbersList.Add(123);

Console.WriteLine(numbersList.Contains(987));
Console.WriteLine(numbersList.Last());

List<int> l = new List<int>(5);
l.Add(1);
Console.WriteLine(l[0]);
l[0] = 15;
Console.WriteLine(l[0]);
//l.Add(1);

List<string> stringList;
//stringList = new List<string>{"ana", "maria"};
stringList = new List<string>();
stringList.Add("ana");

// class assignement
//1
//Declare a list that will have 5 integer elements (12, 14, 34, 567, 34).
//Add those 5 elements into the list one by one. Print the value of the second element in the list.
Console.WriteLine();
Console.WriteLine("Ex1");
List<int> l4 = new List<int>();
l4.Add(12);
l4.Add(14);
l4.Add(34);
l4.Add(567);
l4.Add(34);
Console.WriteLine(l4[1]);

//2
//Declare a list that will contain 3 names (Ana, Victor, Luca).
//Print all the three names comma separated on the same line.
// Ana, Victor, Luca
Console.WriteLine();
Console.WriteLine("Ex2"); 
List<string> l5 = new List<string> { "Ana", "Victor", "Luca" };
Console.WriteLine(l5[0] + ", " + l5[1] + ", " + l5[2]);
Console.WriteLine("use string.Join method: " + string.Join(",", l5));


Console.WriteLine();
Console.WriteLine("--------------------------------------- queues FIFO ----------------------------------");

//Queue<int> numbersQueue;
//Queue<int> numbersQueue = new Queue<int>();
Queue<int> numbersQueue = new Queue<int>(5);

numbersQueue.Enqueue(123);//add
numbersQueue.Enqueue(45);//add
numbersQueue.Dequeue();//remove => 123

//x = numbersQueue[1]; // it does not work like it does on arrays and lists
x = numbersQueue.ElementAt(0);
Console.WriteLine(x);//45
y = numbersQueue.Peek();//elementul de pe pozitia 0 = care a intrat primul in Queue
Console.WriteLine(y);//45

// class assignement
//1
//Declare a queue that will have 5 integer elements (12, 14, 34, 567, 34).
//Add those 5 elements into the queue one by one. Print the value of the second element in the queue.
Console.WriteLine();
Console.WriteLine("Ex1");
Queue<int> q1 = new Queue<int>();
q1.Enqueue(12);
q1.Enqueue(14);
q1.Enqueue(34);
q1.Enqueue(567);
q1.Enqueue(34);
Console.WriteLine(q1.ElementAt(1));

//2
//From the queue above, remove the first two elements. Then add another one,
//whichever you want, and then print the new peek of the queue.
Console.WriteLine();
Console.WriteLine("Ex2");
q1.Dequeue();
q1.Dequeue();
q1.Enqueue(4567);
Console.WriteLine(q1.Peek());

Console.WriteLine();
Console.WriteLine("------------------------------- stacks LIFO ----------------------------------------");

//Stack<int> numbersStack;
//Stack<int> numbersStack = new Stack<int>();
Stack<int> numbersStack = new Stack<int>(5);

numbersStack.Push(3);
numbersStack.Pop();
numbersStack.Push(4);
numbersStack.Push(5);
numbersStack.Push(6);
Console.WriteLine(y);
x = numbersStack.ElementAt(2);
y = numbersStack.Peek();

Console.WriteLine(x);//4
Console.WriteLine(y);//6

// class assignement
//1
//Declare a stack that will have 5 integer elements (12, 14, 34, 567, 34).
//Add those 5 elements into the stack one by one.Print the value of the second element in the stack.
Console.WriteLine();
Console.WriteLine("Ex1");
Stack<int> s1 = new Stack<int>();
s1.Push(12);
s1.Push(14);
s1.Push(34);
s1.Push(567);
s1.Push(34);
Console.WriteLine(s1.ElementAt(1));

//2
//From the stack above, remove the first two elements. Then add another one, whichever
//you want, and then print the new peek of the stack.
Console.WriteLine();
Console.WriteLine("Ex2");
s1.Pop();
s1.Pop();
s1.Push(4567);
Console.WriteLine(s1.Peek());

Console.WriteLine();
Console.WriteLine("------------------------------------------ dictionaries -----------------------------");

//Dictionary<int, string> elements;
//Dictionary<int, string> elements = new Dictionary<int, string>();
Dictionary<int, string> elements = new Dictionary<int, string> { { 12, "ana" }, { 763, "luca" }, { 45, "ioan" } };

elements.Add(2, "maria");
elements.Remove(2);
string element = elements[12];// ana; elements[12]=> it's not the index 12, it's the key 12=> whose value is "ana"

Console.WriteLine(element);

// does not work
//string p = elements[980];	// this does not work because the key '980' does not exist in the dictionary	
//elements.Add(45, "ioana"); // this does not work because the key '45' already exists in the dictionary

// list vs. dictionary
List<int> l10 = new List<int> { 3, 5, 2, 13, 7, 10 };
Dictionary<int, string> elements_1 = new Dictionary<int, string> { { 123, "ana" }, { 234, "maria" }, { 345, "ioana" } };
Console.WriteLine(elements_1[123]); // ana

// dictionary that has both key and value of type string
Dictionary<string, string> d = new Dictionary<string, string>();
d.Add("a", "ana");
d.Add("b", "banana");
d.Add("e", "ene");

// useful things that can be extracted from a dictionary
Console.WriteLine(d["e"]); // ene
Console.WriteLine(d.ElementAt(0).Key); // a
Console.WriteLine(d.ContainsKey("j")); // false
Console.WriteLine(d.ContainsValue("banana")); //true

// dictionary key and value data types can be simple or complex
Dictionary<int, List<string>> d2 = new Dictionary<int, List<string>>();
List<string> letters = new List<string> { "a", "b", "c" };

d2.Add(45, letters);
d2.Add(89, new List<string> { "x", "y" });
Console.WriteLine(d2[45][0]); // a

Dictionary<string, List<string>> d3 = new Dictionary<string, List<string>>();
d3.Add("nico", new List<string> { "x", "y" });
Console.WriteLine(d3["nico"][1]); // y

l = new List<int>();
l.Add(1);
l.Add(2);

List<int> l1 = new List<int>();
l1.Add(10);
l1.Add(20);

List<List<int>> l2 = new List<List<int>>();
l2.Add(l);
l2.Add(l1);

Console.WriteLine(l2[0]); // this prints 'System.Collections.Generic.List`1[System.Int32]' which is the type of the first element in the list
Console.WriteLine(l2[0][0]); // 1
Console.WriteLine(l2[1][1]); // 20

// class assignement
//1
//Declare a dictionary that will have 5 elements (key value pair of int and string, grade and child name).
//Add those 5 elements into the dictionary one by one. Print the value of one of the elements in the dictionary.
Console.WriteLine();
Console.WriteLine("Ex1");
Dictionary<int, string> dict6 = new Dictionary<int, string>();
dict6.Add(5, "Ana");
dict6.Add(6, "Ioan");
dict6.Add(10, "Maria");
dict6.Add(8, "Teodora");
dict6.Add(9, "Mihaela");
Console.WriteLine(dict6[8]); // Teodora

//2
//Declare a dictionary that will contain 5 elements (key value pair of int and a list of strings, grade and children names).
//Print the name of the first child in the dictionary that got a 10 and the second child that got an 8.
Console.WriteLine();
Console.WriteLine("Ex2");
Dictionary<int, List<string>> dict1 = new Dictionary<int, List<string>>
                    {
                        { 5, new List<string>() { "Ana" }  },
                        { 6, new List<string>() { "Ioana", "Ioan" }  },
                        { 8, new List<string>() { "Diana", "Andrei", "Laura" }  },
                        { 10, new List<string>() { "Mihaela" }  },
                    };

Console.WriteLine(dict1[10][0]); //Mihaela
Console.WriteLine(dict1[8][1]); //Andrei



Console.WriteLine("================================ HOMEWORK_DATA_STRUCTURES =====================================");
Console.WriteLine();

/*
 * 1. Store in a variable a number (type float) representing a length value in kilometers. 
 * Convert it to miles and print the result. Then store a number of miles in a variable, 
 * convert it to kilometers and print the value.

Hints: there are 1.61 km in 1 mile
 you may even declare a constant to store this conversion rate
pay attention to the naming of your variables
1 m----------------1.61 km
15 m------------------x
 */

float kilometers = 100f;
float miles = 25f;
float conversionRate = 1.61f;

float kilometersToMiles = kilometers / conversionRate;
Console.WriteLine(kilometersToMiles);

float milesToKilometers = miles * conversionRate;
Console.WriteLine(milesToKilometers);


/*
 * 2. Store 2 string values in two different variables, then print the length of each one,
 * and also print True if the lengths are equal, or False otherwise.
 * Hint: variableName.Lenght C# functionallity might help you & one comparison between 
 * the values would be enough (you don't need to use the "if" clause as we didn't talk about it yet)
 */

//SOLUTION_1
string firstVar = "mara";
string secondVar = "maram";
string thirdVar = "mira";
if (firstVar.Length == secondVar.Length) {
    Console.WriteLine(true);
}
else
    Console.WriteLine(false);

//SOLUTION_2
Console.WriteLine();
Console.WriteLine("Exercise 2");
string firstString = "word";
string secondString = "another word";

Console.WriteLine($"first string length: {firstString.Length}");
Console.WriteLine($"second string length: {secondString.Length}");
Console.WriteLine("the two strings above have the same lenght: " + (firstString.Length == secondString.Length));


/*
 3. Create a list variable that can hold many Romanian city names. Add at least three names in this list. 
Print all of them.
 */

List<string> romanianCities = new List<string>();
romanianCities.Add("Iasi");
romanianCities.Add("Brasov");
romanianCities.Add("Oradea");
romanianCities.Add("Timisoara");
Console.WriteLine(romanianCities[0]);
Console.WriteLine(romanianCities[1]);
Console.WriteLine(romanianCities[2]);
Console.WriteLine(romanianCities[3]);
//=================================================================================
List<int> intList= new List<int>();
intList.Add(1);
intList.Add(2);
intList.Add(3);
intList.Add(4);
intList.Add(5);

List<string> stringList_2 = new List<string>();
stringList_2.Add(firstVar);
stringList_2.Add(secondVar);
stringList_2.Add(thirdVar);
//=================================================================================

/*
 4. Create an array which can store five prices from a store. Add those five prices one by one. 
Print the third number with a discount of 15%.
 */

double[] storePrices = new double[5];
storePrices[0] = 4.80;
storePrices[1] = 7.50;
storePrices[2] = 8.10;
storePrices[3] = 25.50;
storePrices[4] = 100.00;
Console.WriteLine("the third number with a discount of 15% results in " + (storePrices[2] - 15 * storePrices[2]/100));
Console.WriteLine(storePrices[2]*0.85);


/*
 5. Create a stack with recipe names. Act like you cooked all of them and remove them 
one by one from the list after printing them.
 */

Stack<string> recipesNames = new Stack<string>(new string[] 
{"fried eggs", "salted cake", "white chocolate cake", "pizza", "pasta"}); //LIFO
//Stack<string> recipesNames = new Stack<string>();
//recipesNames.Push("fried eggs");
//recipesNames.Push("pizza");
//recipesNames.Push("pasta");
//recipesNames.Push("white chocolate cake");
//recipesNames.Push("avocado with cheese");
Console.WriteLine(recipesNames.Peek());
recipesNames.Pop();
Console.WriteLine(recipesNames.Peek());
recipesNames.Pop();
Console.WriteLine(recipesNames.Peek());
recipesNames.Pop();
Console.WriteLine(recipesNames.Peek());
recipesNames.Pop();
Console.WriteLine(recipesNames.Peek());
recipesNames.Pop();
//Console.WriteLine(recipesNames.Peek());

/*
 * 6. Put all your friend's names in a queue in the order you would like to see them this week. 
 * Display the name of the second one.
 */

Queue<string> friendsNames = new Queue<string>(); //FIFO
friendsNames.Enqueue("Aaaaaa");
friendsNames.Enqueue("Bbbbbb");
friendsNames.Enqueue("Cccccc");
friendsNames.Enqueue("Dddddd");
friendsNames.Enqueue("Eeeeee");
Console.WriteLine(friendsNames.ElementAt(1));


/*
 7. Create a dictionary that holds a personal identification number (line a "CNP") as key and the address of the 
person having this identification number as value. 
Create a second dictionary that holds an integer number as key and a list of strings as value. 
Print every element of every list.
 */

Dictionary<int, string> idAndAddress = new Dictionary<int, string>();
idAndAddress.Add(150, "Iasi, str. Florilor nr. 21");
idAndAddress.Add(277, "Oradea, str. Aviatorilor nr. 37");
idAndAddress.Add(456, "Arad, str. Pietei nr. 9");


Dictionary<int, List<string>> booksPrices = new Dictionary<int, List<string>>();
booksPrices.Add(52, new List<string>{"First Book", "Other book"});
booksPrices.Add(24, new List<string> { "Book 2", "Book 3", "Book 4" });
booksPrices.Add(119, new List<string> {"Book 5" });
Console.WriteLine(booksPrices[52][0]);
Console.WriteLine(booksPrices[52][1]);
Console.WriteLine(booksPrices[24][0]);
Console.WriteLine(booksPrices[24][1]);
Console.WriteLine(booksPrices[24][2]);
Console.WriteLine(booksPrices[119][0]);

